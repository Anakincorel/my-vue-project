// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Vuelidate from 'vuelidate';
import App from './App';
import router from './router';
import store from './vuex';

import { CHECK_AUTH } from "./vuex/types";
import ApiService from "./common/api.service";

import CButton from "./components/UI/CButton"
import InputComponent from "./components/Inputs/InputComponent"
import Pagination from "./components/Pagination/Pagination"
import VeSelect from "./components/UI/VeSelect"
import VeOption from "./components/UI/VeOption"

Vue.use(Vuelidate);

Vue.component('c-button',CButton);
Vue.component('input-component',InputComponent);
Vue.component('pagination',Pagination);
Vue.component('ve-select', VeSelect);
Vue.component('ve-option', VeOption);

Vue.config.productionTip = false;

// Filters
Vue.filter('dateConversion', (value) => {
  const date = value
    .toString()
    .slice(0, 10)
    .replace(new RegExp('-', 'g'), '.');
  const time = value.toString().slice(11, 16);
  return `${date} ${time}`;
});

ApiService.init();

router.beforeEach((to, from, next) => {
  const signPages = ['/sign-in', '/sign-up', '/password'];
  const authRequired = !signPages.includes(to.path);
  const loggedIn = store.state.session.model.authenticated;

  Promise.all([store.dispatch(CHECK_AUTH)]).then(next);

  if (authRequired && !loggedIn) {
    next('sign-in');
  } else if (!authRequired && loggedIn) {
    next('tasks');
  }

  next();
});


/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
});
