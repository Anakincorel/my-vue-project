import Vue from 'vue';
import Router from 'vue-router';
import ForgotPassword from '../components/Pages/LoginScreens/ForgotPassword'
import SignIn from '../components/Pages/LoginScreens/SignIn'
import SignUp from '../components/Pages/LoginScreens/SignUp'
import Articles from '../components/Pages/Articles/Articles'
import PaymentOut from '../components/Pages/Articles/ArticleList'
import APIList from '../components/Pages/APIList/APIList'
import TaskEdit from '../components/Pages/TaskEdit/TaskEdit'
import Settings from '../components/Pages/Profile/Settings'
import ArticlePage from '../components/Pages/Articles/ArticlePage'
import User from '../components/Pages/User/UserPage'
import NewArticle from '../components/Pages/Articles/NewArticle'

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/sign-in',
      name: 'sign-in',
      component: SignIn
    },
    {
      path: '/sign-up',
      name: 'sign-up',
      component: SignUp
    },
    {
      path: '/password',
      name: 'password',
      component: ForgotPassword
    },
    {
      path: '/articles',
      name: 'articles',
      component: Articles
    },
    {
      path: '/result',
      name: 'result',
      component: PaymentOut
    },
    {
      path: '/tasks',
      name: 'tasks',
      component: APIList
    },
    {
      path: '/task-edit/:id',
      name: 'task-edit',
      component: TaskEdit
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings
    },
    {
      path: '/new',
      name: 'new',
      component: NewArticle
    },
    {
      path: '/user/:username',
      name: 'user',
      component: User
    },
    {
      path: '/task-edit',
      name: 'task-edit',
      component: TaskEdit
    },
    {
      path: '/article/:slug',
      name: 'article',
      component: ArticlePage
    },
    {
      path: '/',
      redirect: 'sign-in'
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
});

export default router;
