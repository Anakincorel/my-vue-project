export default {
  tasks: {
    list: [],
    count: null,
    total: 0,
    params: {
      page: 1,
      sort: 'date',
      order: 'asc'
    }
  },
  task: null,
};
