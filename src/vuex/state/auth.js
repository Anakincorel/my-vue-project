import { getToken } from '../../common/jwt.service';
// import { loadToken } from '../../utils';

export default {
  // defaultImage: 'https://www.w3schools.com/howto/img_avatar.png',
  session: {
    loading: false,
    model: {
      user: {
        id: null
      },
      authenticated: !!getToken(),
      token: getToken()
    }
  }
}
