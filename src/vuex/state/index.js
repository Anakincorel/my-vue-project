import posts from './posts';
import tasks from './tasks';
import articles from './articles';
import user from './user';
import auth from './auth';

const state = Object.assign(
  {
    error: {
      title: null,
      message: null,
      error: null
    }
  },
  posts,
  tasks,
  articles,
  user,
  auth
);

export default state;
