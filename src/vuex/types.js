// COMMON MUTATIONS
export const GO_OVER = 'goOver';
export const GO_EXIT = 'goExit';
export const ERROR = 'error';
export const LOADING = 'loading';
export const LOADED = 'loaded';

export const SET_PARAMS = 'setParams';
export const UPDATE_PARAMS = 'updateParams';

export const SET_TOTAL = 'setTotal';
export const SET_LIST = 'setList';
export const ADD_LIST_ITEMS = 'addListItems';
export const REMOVE_LIST_ITEM = 'removeListItem';
export const UPDATE_LIST_ITEM = 'updateListItem';

export const SET_MODEL = 'setModel';
export const UPDATE_MODEL = 'updateModel';
export const SET_PROPERTY = 'setProperty';
export const UPDATE_PROPERTY = 'updateProperty';
export const ADD_PROPERTY_ITEM = 'addPropertyItem';
export const REMOVE_PROPERTY_ITEM = 'removePropertyItem';

// POSTS
export const POSTS = 'posts';
export const POST = 'post';
export const FETCH_POSTS = 'fetchPosts';
export const FETCH_POST = 'fetchPost';
export const SAVE_POST = 'savePost';
export const DELETE_POST = 'deletePost';

// TASKS
export const TASK = 'task';
export const TASKS = 'tasks';
export const FETCH_TASK = 'fetchTask';
export const FETCH_TASKS = 'fetchTasks';
export const CREATE_TASK = 'createTask';
export const DELETE_TASK = 'deleteTask';
export const FETCH_TASKS_COUNT = 'fetchTasksCount';
export const SET_TASKS_COUNT = 'setTasksCount';
export const FETCH_TOTAL = 'fetchTotal';
export const SET_TASKS_PARAMS = 'setTasksParams';

// USER
export const FETCH_USER = 'fetchUser';


// ARTICLES
export const FETCH_ARTICLE = "fetchArticle";
export const FETCH_ARTICLES = "fetchArticles";
export const CREATE_ARTICLE = "createArticle";

export const ARTICLE_PUBLISH = "publishArticle";
export const ARTICLE_DELETE = "deleteArticle";
export const ARTICLE_EDIT = "editArticle";
export const ARTICLE_EDIT_ADD_TAG = "addTagToArticle";
export const ARTICLE_EDIT_REMOVE_TAG = "removeTagFromArticle";
export const ARTICLE_RESET_STATE = "resetArticleState";
export const CHECK_AUTH = "checkAuth";
export const COMMENT_CREATE = "createComment";
export const COMMENT_DESTROY = "destroyComment";
export const FAVORITE_ADD = "addFavorite";
export const FAVORITE_REMOVE = "removeFavorite";

export const FETCH_COMMENTS = "fetchComments";
export const FETCH_PROFILE = "fetchProfile";
export const FETCH_PROFILE_FOLLOW = "fetchProfileFollow";
export const FETCH_PROFILE_UNFOLLOW = "fetchProfileUnfollow";
export const FETCH_TAGS = "fetchTags";
export const LOGIN = "login";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const UPDATE_USER = "updateUser";

// MUTATIONS
export const FETCH_END = "setArticles";
export const FETCH_START = "setLoading";
export const PURGE_AUTH = "logOut";
export const SET_ARTICLE = "setArticle";
export const SET_AUTH = "setUser";
export const SET_COMMENTS = "setComments";
export const SET_ERROR = "setError";
export const SET_PROFILE = "setProfile";
export const SET_TAGS = "setTags";
export const TAG_ADD = "addTag";
export const TAG_REMOVE = "removeTag";
export const UPDATE_ARTICLE_IN_LIST = "updateAricleInList";
export const RESET_STATE = "resetModuleState";

