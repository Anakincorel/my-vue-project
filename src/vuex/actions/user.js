import { FETCH_PROFILE, SET_MODEL } from '../types';

export default {
  [FETCH_PROFILE]: ({ commit, getters}, params) => getters
      .GET(`/profiles/${params.username}`)
      .then(data => {
        commit(SET_MODEL, { name: 'profile', model: data.profile });
      })
}
