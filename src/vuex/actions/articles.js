import * as type from '../types';
import ApiService from '../../common/api.service';
import router from '../../router'
import { throwError } from '../../utils';

export default {
  [type.FETCH_ARTICLES]: ({ commit, getters }, params) => {
    commit(type.FETCH_START);
    return getters
      .GET(`/articles`, params.filters)
      .then(list => {
        commit(type.SET_LIST, { name: 'articles', list });
        commit(type.FETCH_END);
      })
  },

  [type.FETCH_ARTICLE]: ({ commit, getters }, params) => getters
    .GET(`/articles/${params.slug}`)
    .then( model => {
      commit(type.SET_MODEL, { name: 'article',model: model.article});
    }),

  [type.CREATE_ARTICLE]: ({ commit }, article) => {
    ApiService.setHeader();
    ApiService.post('articles', {article})
      .then(router.push({name: 'articles'}))
      .catch(throwError(commit, 'Ошибка создания статьи'));
  }


}
