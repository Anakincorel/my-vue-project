import * as type from '../types';
import { throwError } from '../../utils';

export default {
  [type.FETCH_TASKS]: ({ commit, getters }, params ) => {
    const name = type.TASKS;
    return getters
      .GET('/data', {
        _page: params.page,
        _limit: params.limit,
        _sort: params.sort,
        _order: params.order
      })
      .then(list => {
        commit(type.SET_LIST, { name, list });
      })
      .catch(throwError(commit, 'Ошибка получения списка задач'));
  },
  [type.FETCH_TOTAL]: ({commit, getters}) => {
    const name = 'tasks';
    return getters
      .GET('/data')
      .then(data => {
        commit(type.SET_TOTAL, { name, total: data.length })
      })
  },
  [type.CREATE_TASK]: ({ commit, getters }, task) => {
    if (!task.id) {
      return getters
        .POST('/data', task)
        .catch(throwError(commit, 'Ошибка отправки данных'))
    }
    return getters
      .PUT(`/data/${task.id}`, task)
      .then()
      .catch(throwError(commit, 'Ошибка отправки данных'))
  },
  [type.DELETE_TASK]: ({ commit, getters }, id) =>
    getters
      .DELETE(`/data/${id}`)
      .catch(throwError(commit, 'Ошибка получения данных задачи')),
  [type.FETCH_TASK]: ({ commit, getters }, id) =>
    getters
      .GET(`/data/${id}`)
      .catch(throwError(commit, 'Ошибка получения данных задачи')),
  [type.SET_TASKS_PARAMS]: ({ commit }, { page, sort, order }) => {
    commit(type.SET_TASKS_PARAMS, { page, sort, order });
  }
};
