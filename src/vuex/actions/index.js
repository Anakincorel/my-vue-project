import posts from './posts';
import tasks from './tasks';
import articles from './articles';
import user from './user';
import auth from './auth';

const actions = Object.assign({}, posts, tasks, articles, user, auth);

export default actions;
