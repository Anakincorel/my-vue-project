import { CHECK_AUTH, LOGIN, LOGOUT, PURGE_AUTH, REGISTER, SET_AUTH, UPDATE_USER } from '../types';
import { getToken } from '../../common/jwt.service';
import ApiService from '../../common/api.service';
import { throwError } from '../../utils';

export default {
  [LOGIN](context, credentials) {
    return new Promise(resolve => {
      ApiService.post('users/login', { user: credentials })
        .then(({ data }) => {
          context.commit(SET_AUTH, data.user);
          resolve(data);
        })
        .catch(throwError(context.commit, 'Ошибка авторизации'));
    });
  },
  [LOGOUT](context) {
    context.commit(PURGE_AUTH);
  },
  [REGISTER](context, credentials) {
    return new Promise((resolve) => {
      ApiService.post('users', { user: credentials })
        .then(({ data }) => {
          context.commit(SET_AUTH, data.user);
          resolve(data);
        })
        .catch(throwError(context.commit, 'Ошибка регистрации'));
    });
  },
  [CHECK_AUTH](context) {
    if (getToken()) {
      ApiService.setHeader();
      ApiService.get('user')
        .then(({ data }) => {
          context.commit(SET_AUTH, data.user);
        })
        .catch(throwError(context.commit, 'Ошибка авторизации'));
    } else {
      context.commit(PURGE_AUTH);
    }
  },
  [UPDATE_USER](context, payload) {
    const { email, username, password, image, bio } = payload;
    const user = {
      email,
      username,
      bio,
      image
    };
    if (password) {
      user.password = password;
    }

    return ApiService.put('user', user).then(({ data }) => {
      context.commit(SET_AUTH, data.user);
      return data;
    });
  }
}
